# elastic-stack-swarm

Set up cluster for elastic-stack by using docker swarm.

![portainer](assets/images/portainer-swarm.png)

# Environment
```
$ docker -v
Docker version 20.10.5, build 55c4c88
$ docker-compose -v
docker-compose version 1.28.5, build c4eb3a1f
$ docker-machine -v
docker-machine version 0.16.2, build bd45ab13

% docker-machine ls
NAME                      ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER      ERRORS
elasticsearch-data-01     -        virtualbox   Running   tcp://192.168.99.111:2376           v19.03.12   
elasticsearch-data-02     -        virtualbox   Running   tcp://192.168.99.112:2376           v19.03.12   
elasticsearch-data-03     -        virtualbox   Running   tcp://192.168.99.113:2376           v19.03.12   
elasticsearch-master-01   -        virtualbox   Running   tcp://192.168.99.106:2376           v19.03.12   
elasticsearch-master-02   -        virtualbox   Running   tcp://192.168.99.107:2376           v19.03.12   
elasticsearch-master-03   -        virtualbox   Running   tcp://192.168.99.108:2376           v19.03.12   
filebeat-01               -        virtualbox   Running   tcp://192.168.99.117:2376           v19.03.12   
filebeat-02               -        virtualbox   Running   tcp://192.168.99.118:2376           v19.03.12   
kibana                    -        virtualbox   Running   tcp://192.168.99.119:2376           v19.03.12   
local                     *        virtualbox   Running   tcp://192.168.99.105:2376           v19.03.12   
logstash-01               -        virtualbox   Running   tcp://192.168.99.114:2376           v19.03.12   
logstash-02               -        virtualbox   Running   tcp://192.168.99.115:2376           v19.03.12   
logstash-03               -        virtualbox   Running   tcp://192.168.99.116:2376           v19.03.12
```
Hardware resources
```
$ system_profiler SPHardwareDataType
Hardware:

    Hardware Overview:

      Model Name: MacBook Pro
      Model Identifier: MacBookPro15,1
      Processor Name: 8-Core Intel Core i9
      Processor Speed: 2.4 GHz
      Number of Processors: 1
      Total Number of Cores: 8
      L2 Cache (per Core): 256 KB
      L3 Cache: 16 MB
      Hyper-Threading Technology: Enabled
      Memory: 32 GB
```
# Development
## Create docker-machine

```
$ docker-machine create --driver virtualbox elasticsearch-data-01
```

## Check join-token

```
manager $ docker swarm join-token worker
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-1xopvt9ri58lpbp9poi9bg3du70om35k6mwfctn7cnodjnmbne-cgz0z3783of0o3ftslt4pdbn3 192.168.99.105:2377
```

## Add a worker to this swarm

```
$ eval "$(docker-machine env elasticsearch-data-01)"
worker $ docker swarm join --token SWMTKN-1-1xopvt9ri58lpbp9poi9bg3du70om35k6mwfctn7cnodjnmbne-cgz0z3783of0o3ftslt4pdbn3 192.168.99.105:2377
```

## Set memory options for elasticsearch

```
$ docker-machine ssh elasticsearch-data-01
worker $ sudo vi /var/lib/boot2docker/profile
worker $ sudo sysctl -w vm.max_map_count=262144
$ exit
$ docker-machine restart elasticsearch-data-01
```

since operating system for docker is boot2docker, you need to edit "boot2docker/profile" to set memlock.

```
$ vim /var/lib/boot2docker/profile

EXTRA_ARGS='
--label provider=virtualbox
--default-ulimit memlock=-1
 
'                                 
CACERT=/var/lib/boot2docker/ca.pem 
DOCKER_HOST='-H tcp://0.0.0.0:2376'
DOCKER_STORAGE=overlay2
DOCKER_TLS=auto                              
SERVERKEY=/var/lib/boot2docker/server-key.pem
SERVERCERT=/var/lib/boot2docker/server.pem
```

## Add node label
Set node label to add placement constraint. You can also add labels from portainer.io container.
```
manager $ docker node update --label-add es-data=true elasticsearch-data-01
```

## Deploy
```
$ docker stack deploy --compose-file docker-compose.yml mystack
```

## Delete stack
```
$ docker stack rm mystack
```

## Get status of stack

```
manager $ docker stack ps mystack
ID             NAME                                                 IMAGE                                                  NODE                      DESIRED STATE   CURRENT STATE            ERROR     PORTS
gobn4opuimfy   mystack_es-data.1                                    docker.elastic.co/elasticsearch/elasticsearch:7.12.0   elasticsearch-data-01     Running         Running 28 seconds ago             
l1nin8e1km0t   mystack_es-data.2                                    docker.elastic.co/elasticsearch/elasticsearch:7.12.0   elasticsearch-data-03     Running         Running 28 seconds ago             
r34nklfol95l   mystack_es-data.3                                    docker.elastic.co/elasticsearch/elasticsearch:7.12.0   elasticsearch-data-02     Running         Running 28 seconds ago             
30qqi9shzegy   mystack_es-master.1                                  docker.elastic.co/elasticsearch/elasticsearch:7.12.0   elasticsearch-master-03   Running         Running 31 seconds ago             
vvjuyionyqtb   mystack_es-master.2                                  docker.elastic.co/elasticsearch/elasticsearch:7.12.0   elasticsearch-master-01   Running         Running 31 seconds ago             
a70ynoshl7cb   mystack_es-master.3                                  docker.elastic.co/elasticsearch/elasticsearch:7.12.0   elasticsearch-master-02   Running         Running 31 seconds ago             
uocudnm116hm   mystack_kibana.1                                     docker.elastic.co/kibana/kibana:7.12.0                 kibana                    Running         Running 24 seconds ago             
wvk01algzsy4   mystack_logstash.1                                   docker.elastic.co/logstash/logstash:7.12.0             logstash-02               Running         Running 16 seconds ago             
j21o4hr1mn3f   mystack_logstash.2                                   docker.elastic.co/logstash/logstash:7.12.0             logstash-03               Running         Running 16 seconds ago             
rw7tlzei5ywh   mystack_logstash.3                                   docker.elastic.co/logstash/logstash:7.12.0             logstash-01               Running         Running 16 seconds ago             
ksd16z2ngg58   mystack_metricbeat.1                                 docker.elastic.co/beats/metricbeat:7.12.0              local                     Running         Running 10 seconds ago             
vllroxowbin4   mystack_portainer_master.y201dtu6z4eb722e04xidm9ol   portainer/portainer:latest                             local                     Running         Running 7 seconds ago              
                                    
```

## Get service information
```
$ docker service ls
ID             NAME                       MODE         REPLICAS               IMAGE                                                  PORTS
vu39p5h1ciu8   mystack_es-data            replicated   3/3 (max 1 per node)   docker.elastic.co/elasticsearch/elasticsearch:7.12.0   
9l06iajhebor   mystack_es-master          replicated   3/3 (max 1 per node)   docker.elastic.co/elasticsearch/elasticsearch:7.12.0   
iv446tehe3oc   mystack_kibana             replicated   1/1                    docker.elastic.co/kibana/kibana:7.12.0                 *:5601->5601/tcp
tjaen6ecqdmc   mystack_logstash           replicated   3/3 (max 1 per node)   docker.elastic.co/logstash/logstash:7.12.0             
vkse7irszqmw   mystack_metricbeat         replicated   1/1                    docker.elastic.co/beats/metricbeat:7.12.0              
lslw1vi4poae   mystack_portainer_master   global       1/1                    portainer/portainer:latest                             *:9000->9000/tcp
```

## Generate filebeat logs
Usage of `log-generator`

```
$ cd beats/filebeat/helper
$ go run log-generator.go -h
Usage of log-generator:
  -d int
        duration (default 60)
  -p int
        period (default 1)
```

Create logs
* Create logs in `beats/filebeat/node-0*/logs/`.
* Create logs for node-01 and node-02 in same time.
* duration : (default) 60 sec
* period : (default) 1 log / sec

```
$ cd beats/filebeat/helper 
$ go run log-generator.go -d 10    
[2021-03-28 20:34:33]create log.
[2021-03-28 20:34:34]create log.
[2021-03-28 20:34:35]create log.
[2021-03-28 20:34:36]create log.
[2021-03-28 20:34:37]create log.
[2021-03-28 20:34:38]create log.
[2021-03-28 20:34:39]create log.
[2021-03-28 20:34:40]create log.
[2021-03-28 20:34:41]create log.
[2021-03-28 20:34:42]create log.
```
